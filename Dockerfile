FROM debian:10
RUN apt-get update && apt-get upgrade -y
RUN apt-get install curl gnupg gnupg2 gnupg2 -y
RUN echo 'deb http://download.opensuse.org/repositories/isv:/ownCloud:/server:/10/Debian_10/ /' | tee /etc/apt/sources.list.d/isv:ownCloud:server:10.list
RUN curl -fsSL https://download.opensuse.org/repositories/isv:ownCloud:server:10/Debian_10/Release.key | gpg --dearmor | tee /etc/apt/trusted.gpg.d/isv_ownCloud_server_10.gpg > /dev/null
RUN apt-get update && apt-get upgrade -y
RUN apt install owncloud-complete-files
RUN cp -r /var/www/owncloud /var/www/html
RUN usermod -aG www-data www-data
RUN chown -R www-data:www-data /var/www/html
RUN chmod -R 775 /var/www/html
RUN apt-get install -y apache2 libapache2-mod-php php-gd php-json php-mysql php-sqlite3 php-curl php-intl php-imagick php-zip php-xml php-ldap php-apcu php-curl php-intl php-xml php-mbstring php-soap php-redis php-dev libsmbclient-dev php-gmp smbclient unzip
RUN rm -rf /var/lib/apt/lists/*
EXPOSE 80

CMD ["/usr/sbin/apache2ctl", "-D",  "FOREGROUND"]
